[خرید بیت کوین](https://ok-ex.io/buy-and-sell/BTC) یا فروش آن برای بسیاری از کاربران ارزهای دیجیتال به دلیل اختلال در تراکنش و کارمزدها امر مشکلی است. بیت کوین یک ارز دیجیتال غیرمتمرکز است که بدون واسطه (بانک مرکزی) قابلیت پرداخت مستقیم از کاربری به کاربر دیگر را دارد. مشکلی که کاربران در دنیای ارزهای دیجیتال با آن زیاد مواجه می‌شوند، سرعت پایین انتقال و کارمزدهای زیاد رمزارزها است. انتقال یا خرید و فروش ارزهای بزرگی مثل بیت کوین و اتریوم به دلیل کارمزد بالا و تراکنش‌های زیاد با مشکلاتی مواجه شده است. لایتنینگ راهکاری برای حل این مشکلات است که در ادامه به آن خواهیم پرداخت.

![](https://lh4.googleusercontent.com/Z6PxaL6MBIjLXEmcCTCjWXbhu-4ULqMp0_neIlGb5NWcx1doo6CtjN4sbmY_vRFDnI7gfvCxVmKOyoeX8Uh7WmRBQoXfa9NKLQwRoM3vAqc9-z6CEUuVPLC3YjMCFxmdrcAMJEFB)

## بیت کوین

با گذشت سال‌های زیاد از رونق بازر ارزهای دیجیتال همچنان بسیاری ارزهای دیجیتال را با بیت کوین می‌شناسند. برای اولین بار در سال 2008 سازنده بیت کوین یعنی ساتوشی ناکاتومو ارز دیجیتالی را معرفی کرد که بدون نیاز به بانک مرکزی، امکان انتقال آن وجود داشت. خرید بیت کوین یا فروش آن در ابتدا با مشکل رو به رو بود و بسیاری آن را یک حباب سوداگرانه توصیف می‌کردند که فاقد اعتبار بود. اما با گذشت سال‌ها از آن زمان، بیت کوین به عنوان بزرگترین و با ارزش‌ترین رمزارز درحال حاضر دنیا شناخته می‌شود.

تراکنش‌های این رمز ارز محبوب بدون نیاز به واسطه یا بانک مرکزی انجام‌ می‌شود و می‌توان آن را از شخصی به شخص دیگر انتقال داد. تراکنش‌ها در یک شبکه به صورت رمزنگاری شده تایید می‌شود که در یک دفتر کل عمومی به نام بلاک چین ثبت می‌شود.

## شبکه لایتنینگ

قبل از تعریف شبکه لایتنینگ باید با تعریف بلاک چین آشنا شوید. بلاک چین به معنیه زنجیره‌ای از بلوک‌ها است. در هرکدام از این بلاک‌ها اطلاعاتی ذخیره می‌شوند که به بلاک‌های بعدی ارتباط دارند و در واقع همه آن‌ها مثل زنجیر به یکدیگر متصل هستند. بلاک چین طوری طراحی شده است که در هر بلاک تعداد مشخصی تراکنش قرار می‌گیرد.

اما [بلاک چین](https://fa.wikipedia.org/wiki/%D8%B2%D9%86%D8%AC%DB%8C%D8%B1%D9%87_%D8%A8%D9%84%D9%88%DA%A9%DB%8C) نقطه ضعف‌هایی دارد که تراکنش‌های کاربران را با مشکل مواجه کرده است. در بلاک چین اگر تراکنشی در بلاک جاری قرار نگیرد، وارد صف تراکنش‌های درحال انتظار می‌رود. این بلاک ممپول نام دارد و تراکنش‌های مربوط به ممپول ممکن است بین چند دقیقه تا چند روز زمان ببرد.مشکل بعدی برای خرید بیت کوین یا اتریم و فروش آن‌ها، هزینه‌ بالای تراکنش‌ها است.

شبکه بیت کوین با پروتکل اجتماعی (اثبات کار)، کار می‌کند. برای جبران انرژی صرف شده توسط ماینرها، کارمزد تراکنش‌ها به آن‌ها می‌رسد. اگر سیستم کوچیک و تعداد تراکنش‌ها پایین باشد مشکلی وجو ندارد اما با بالا رفتن تراکنش‌ها و شلوغ شدن شبکه، کارمزدها نیز بالاتر می‌رود و در شبکه‌های شلوغی مثل بیت کوین یا اتریوم این کارمزد بسیار بالا است. برای حل این مشکلات به راه حلی به نام لایتنینگ میرسیم.

## شبکه لایتنینگ چیست؟

در زبان انگلیسی لایتنینگ به معنای رعدوبرق می‌باشد و انتخاب نام شبکه لاتنینگ، نشان دهنده سرعت بالای این شبکه است. اگر بخواهیم ساده بگوییم، لایتنینگ به معنای تراکنش‌های سریع و بدون کارمزد در بیت کوین می‌باشد که مشکل بسیاری از کاربران برای خرید بیت کوین را برطرف کرده است.

![](https://lh3.googleusercontent.com/MtiRJB9QwNDNbGeEU0D3y0PyYTjJs6uYy4KESOZs5vuPcy97tyV9OD4MehPjdqs9qw8FUZDQmt9joZ7R64QgNu9IEYYt5CqcCALo0B_tZcLkn2JLvNRzEH4CT554N6GGeEoL9cjF)

## شبکه لایتنینگ چگونه کار می‌کند

در این روش برای پرداخت از سیستم (P2P) استفاده می‌شود که با ایجاد یک کانال پرداختی و وارد کردن مقدار مشخصی بیت کوین، تراکنش‌ها انجام می‌گیرد. در این روش انتقال بین دو نفر و بدون واسطه انجام می‌شود که بسیار سریع و ارزان‌تر از روش‌های دیگر است. علاوه بر این در لایتنینگ می‌توان هر تعداد تراکنش را بصورت دو نفره بدون کارمزد بیشتر انجام داد.

خرید بیت کوین با استفاده از شبکه لایتنینگ، سرعت تراکنش‌ها را بسیار بالا می‌برد. در این روش، پرداخت بصورت آنی تسویه نمی‌شود بلکه درخواست خریدار و فروشنده به ‌سرعت تایید می‌شود. بنا به توافق طرفین در تراکنش، ممکن است تسویه بین چند روز تا چند هفته زمان ببرد. همچنین بهتر است بدانید سیستم لایتنینگ به وسیله گره‎ها (Node) اداره می‌شوند.

شبکه لایتنینگ را می‌توان مثل یک پل تصور کرد که روی شبکه اصلی (بلاک چین) ایجاد شده‌است و به بلاک چین این اجازه را می‌دهد که قدرت و سرعت بیشتری برای پذیرش تراکنش‌ها داشته باشد.

## مزایای شبکه لایتنینگ

اما شبکه لایتنینگ برای خرید بیت کوین یا فروش آن چه مزیت‌های دارد؟ اولین فایده این شبکه این است که ترافیک تراکنش‌های شبکه بیت کوین را کاهش می‌دهد. همانطور که اشاره شد با ایجاد کانال‌های دو طرفه سرعت تراکنش‌ها به صورت چشم گیری افزایش پیدا می‌کند. و در آخر به دلیل کاهش بار شبکه، هزینه تراکنش‌ها با این روش بسیار کم‌تر خواهد بود.

## معایب شبکه لایتنینگ

اولین و به نوعی بزرگترین مشکل این شبکه این است که در صورت آفلان بودن دریافت کننده، امکان دریافت مبلغ وجود ندارد. شبکه لایتنینگ هنوز برای مبالغ بالا مناسب نیست و بیشتر برای تراکنش‌های خرد کاربرد دارد. ایراد بعدی ای شبکه برای خرید بیت کوین، طرز استفاده آن است چون بسیاری از کاربران هنوز با آن آشنایی کافی را ندارند.

## شبکه لایتنینگ و خرید بیت کوین

بطور کلی شبکه لاتنینگ با دور زدن بلاک چین اصلی بیت کوین، کار کرده و سرعت تراکنش‌ها را بالا می‌برد. چون اساس کار شبکه لایتنینگ، کانال‌های پرداختی دو طرفه است پس برای خرید بیت کوین یا فروش آن فقط زمان مبادله بین دو کیف پول (کیف پول‌های چند امضایی) محاسبه می‌شود.

برای استفاده از لایتنینگ کافی است مقداری بیت کوین در یک کانال پرداختی واریز و قفل شود. سپس می‌توان تا قبل از بسته شدن کانال، بیت کوین‌ها را در شبکه لایتنینگ خرج کرد. نکته‌ای که باید به آن توجه کرد این است که در شبکه لایتنیگ چیزی به نام آدرس کیف پول وجود ندارد و کاربران با صورت حساب یا اینویس (Invoice) کار می‌کنند.

درحال حاضر با بهبود کیف پول‌های لایتنینگ، فرایندهای باز کردن کانال‌ها، درخواست پرداخت و ارسال وجه بسیار آسان‌تر از گذشته شده است. در ادامه بهترین کیف‌ پول‌های این شبکه را خواهیم دید.

## کیف پول‌های لایتنینگ بیت کوین

ملاک انتخاب این کیف پول‌ها در درجه اول امنیت و سپس رابط کاربری ساده و کارآمد است. بهترین کیف‌ پول‌های حال حاضر لایتنینگ عبارت‌اند از:

-   اوکی اکسچنج
    
-   والت‌آف ساتوشی
    
-   بلووالت
    
-   مون
    

![](https://lh6.googleusercontent.com/o0AZaRMKU65kS2GRLUZ0m8jhcIdg3dpAb4KnQxd1U2ffgC9r1L_l0jm714r9oYWs7OQXBPB-LpsDlzKkxg37KMGnIEZp1W1sYN_5hneyBJGECotEjpZ99rnfBVGxWcZnXrX5SvUd)

## آینده شبکه لایتنینگ

با پیشرفت تکنولوژی به ویژه در حوزه ارزهای دیجیتال، آرام آرام به سمتی پیش می‌رویم که استفاده از ارزهای دیجیتال به دنیای واقعی خواهند رسید. پیش از این استفاده از بیت کوین بصورت یک ارز رایج، امری دشوار بود چون برای خرید بیت کوین و استفاده از آن علاوه بر تعرفه زیاد باید زمان زیادی نیز صرف می‌شد. حالا با رشد شبکه لایتنینگ انتظار می‌رود سرعت استفاده از بیت کوین و ارزهای دیگر مثل اتریوم در دنیای واقعی، شتاب بیشتری بگیرد. کشور السالوادور اولین کشوری است که بیت کوین را به عنوان ارز رسمی خود پذیرفته است که این امر اولین قدم برای ورود ارزهای دیجیتال به دنیای واقعی به حساب می‌آید.
